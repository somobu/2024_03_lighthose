class_name Player
extends CharacterBody3D

const SPEED = 1.5
const JUMP_VELOCITY = 4.5
const LOOK_OVERRIDE_TIMEOUT = 2.5
const FIRE_TIMEOUT = 0.001

@onready var camera: Camera = $"../Camera"
var projectile_ref = preload("res://projectile.tscn")

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var look_input_timeout = LOOK_OVERRIDE_TIMEOUT

var mouse_look_timeout = LOOK_OVERRIDE_TIMEOUT
var mouse_look_angle = 0

var fire_timeout = FIRE_TIMEOUT

var current_interp_area: InterpArea = null

func _input(event):
	if event is InputEventMouseMotion:
		mouse_look_timeout = 0
		var vp: Viewport = get_viewport()
		var cam_rot_dir = camera.to_global(Vector3(1,0,0))
		mouse_look_angle = (event.global_position - Vector2(vp.size) / 2).angle_to(Vector2(cam_rot_dir.x, cam_rot_dir.z))
	


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	var input_dir = Input.get_vector("move_left", "move_right", "move_fwd", "move_bwd")
	var direction = camera.map_input(input_dir).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		
		$ArrowRed.global_rotation.y = Vector2(direction.x, direction.z).angle_to(Vector2(1,0))
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	
	var look_dir = Input.get_vector("look_left", "look_right", "look_fwd", "look_bwd")
	var look_direction = camera.map_input(look_dir).normalized()
	if look_direction:
		look_input_timeout = 0
		$ArrowGreen.global_rotation.y = Vector2(look_direction.x, look_direction.z).angle_to(Vector2(1,0))
	elif mouse_look_timeout < LOOK_OVERRIDE_TIMEOUT:
		mouse_look_timeout += delta
		$ArrowGreen.global_rotation.y = mouse_look_angle
	else:
		look_input_timeout += delta
		
		if look_input_timeout > LOOK_OVERRIDE_TIMEOUT and direction:
			$ArrowGreen.global_rotation.y = Vector2(direction.x, direction.z).angle_to(Vector2(1,0))
	
	# Clamp look angle
	var body_angle = $ArrowRed.global_rotation.y
	var agl = $ArrowGreen.global_rotation.y - body_angle
	if agl > PI: agl -= 2*PI
	$ArrowGreen.global_rotation.y = clamp(agl, -0.7*PI, 0.7*PI) + body_angle
	
	move_and_slide()
	
	if current_interp_area != null:
		current_interp_area.interp(self, camera)
	
	fire_timeout += delta
	if Input.is_action_pressed("fire") and fire_timeout > FIRE_TIMEOUT:
		fire_timeout = 0
		spawn_projectile()


func spawn_projectile():
	var force_direction = to_local($ArrowGreen.to_global(Vector3(1,0,0))).normalized()
	
	var inst = projectile_ref.instantiate() as RigidBody3D
	get_parent().add_child(inst)
	
	inst.global_position = global_position + Vector3(0, 1.2, 0) + force_direction * 0.3
	inst.apply_central_force(force_direction * 500)
