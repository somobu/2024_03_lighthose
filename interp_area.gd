@tool
class_name InterpArea
extends Area3D

@export var size = Vector3(1,1,1) : set = _set_shape_size

## Camera yaw, in degrees
@export var yaw: Curve = null

## Camera pitch, in degrees
@export var pitch: Curve = null

## Camera distance, in units
@export var dist: Curve = null

## Camera fov, in deg
@export var fov: Curve = null


# Called when the node enters the scene tree for the first time.
func _ready():
	if Engine.is_editor_hint():
		var cs = $CollisionShape3D as CollisionShape3D
		cs.shape = cs.shape.duplicate(true)
		recalc()

func _set_shape_size(value: Vector3):
	if value.x < 0: value.x = 0
	if value.y < 0: value.y = 0
	if value.z < 0: value.z = 0
	
	size = value
	recalc()



func recalc():
	$CollisionShape3D.shape.size = size
	
	$MarkerStart.position.x = -0.5 * size.x
	$MarkerEnd.position.x = 0.5 * size.x



func interp(player: Player, camera: Camera):
	var percent = to_local(player.global_position).x / size.x + 0.5
	
	if yaw != null:		camera.adjust_yaw(deg_to_rad(yaw.sample(percent)))
	if pitch != null:	camera.adjust_pitch(deg_to_rad(90-pitch.sample(percent)))
	if dist != null:	camera.adjust_dist(dist.sample(percent))
	if fov != null:		camera.adjust_fov(fov.sample(percent))


func _on_body_entered(body: Node3D):
	if body is Player:
		body.current_interp_area = self


func _on_body_exited(body: Node3D):
	if body is Player:
		if body.current_interp_area == self:
			body.current_interp_area = null

