extends RigidBody3D

var timeout = 15

func _process(delta):
	timeout -= delta
	if timeout < 0:
		queue_free()
